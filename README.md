# Guidance Portal

Serwis z poradami dostępny z WWW.
system umożliwiający dzielenie się wiedzą, wzorowany na serwisach stackoverflow.com oraz pytamy.pl. Użytkownicy mają możliwość zadawania pytań,
udzielania oraz oceniania odpowiedzi.

Główne funkcje systemu:
Rejestracja kont użytkowników
Dodawanie i usuwanie pytań
Udzielanie i usuwanie odpowiedzi
Możliwość dodawania komentarzy do odpowiedzi
Podział pytań na kategorie

Technologie:  Java 11, Spring, Hibernate
Model danych: [Data_Model](https://drive.google.com/file/d/1rOlkBaN1IcMOzqtydBOJja3xT0HO6A7p/view?usp=sharing)