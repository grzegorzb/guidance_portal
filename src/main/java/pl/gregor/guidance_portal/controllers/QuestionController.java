package pl.gregor.guidance_portal.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.gregor.guidance_portal.dao.AnswerDao;
import pl.gregor.guidance_portal.dao.CategoryDao;
import pl.gregor.guidance_portal.dao.QuestionDao;
import pl.gregor.guidance_portal.entities.Answer;
import pl.gregor.guidance_portal.entities.Category;
import pl.gregor.guidance_portal.entities.Question;

import java.util.List;

@Controller
@RequestMapping("/")
public class QuestionController {

    private final QuestionDao questionDao = new QuestionDao();
    private final AnswerDao categoryDao = new AnswerDao();

    @GetMapping("index")
    public String get(){
        return "index";
    }

    @GetMapping("questionList")
    public String getQuestionList(Model model) {
        Iterable<Question> questions = this.questionDao.getAll();
        model.addAttribute("questions", questions);
        return "questionList";
    }

    @GetMapping("question/{id}")
    public String getQuestion(@PathVariable("id") int questionId,
                                    Model model) {
        Question question = this.questionDao.get(questionId);
        String category_name = question.getCat_ref().getName();
        List<Answer> answers = question.getAnswerList();
        model.addAttribute("question", question);
        model.addAttribute("category_name", category_name);
        model.addAttribute("answers", answers);
        return "question";
    }
}
