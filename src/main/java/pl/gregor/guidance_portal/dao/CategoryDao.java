package pl.gregor.guidance_portal.dao;

import org.springframework.stereotype.Component;
import pl.gregor.guidance_portal.entities.Category;
import pl.gregor.guidance_portal.util.GenericDao;

@Component
public class CategoryDao extends GenericDao<Category> {
}
