package pl.gregor.guidance_portal.dao;

import org.springframework.stereotype.Component;
import pl.gregor.guidance_portal.entities.Question;
import pl.gregor.guidance_portal.util.GenericDao;

@Component
public class QuestionDao extends GenericDao<Question> {
}
