package pl.gregor.guidance_portal.dao;

import org.springframework.stereotype.Component;
import pl.gregor.guidance_portal.entities.User;
import pl.gregor.guidance_portal.util.GenericDao;

@Component
public class UserDao extends GenericDao<User> {
}
