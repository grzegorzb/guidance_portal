package pl.gregor.guidance_portal.dao;

import org.springframework.stereotype.Component;
import pl.gregor.guidance_portal.util.GenericDao;
import pl.gregor.guidance_portal.entities.Answer;

@Component
public class AnswerDao extends GenericDao<Answer> {
}
