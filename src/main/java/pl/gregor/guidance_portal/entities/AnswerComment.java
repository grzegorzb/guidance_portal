package pl.gregor.guidance_portal.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;


/**
 * Question entity
 * @author Grzegorz Bilas
 *
 */

@Getter @Setter @EqualsAndHashCode @NoArgsConstructor
//@Builder( buildMethodName = "build", builderMethodName = "", toBuilder = false, access = AccessLevel.PUBLIC, setterPrefix = "with")
//@Builder(builderClassName = "Question", buildMethodName = "build", builderMethodName = " ", toBuilder = false, access = AccessLevel.PUBLIC, setterPrefix = "with")
@Entity
@Table(name = "answer_comments")
public class AnswerComment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "content_comment")
    private String contentComment;

    @Column(name = "date_time")
    private Date dateTime;

    @ManyToOne
    private Answer answer;

    @ManyToOne
    private User user_co;

    public AnswerComment withId(int id) {
        setId(id);
        return this;
    }

    public AnswerComment withContentComment(String contentComment) {
        setContentComment(contentComment);
        return this;
    }

    public AnswerComment withDateTime(Date dateTime) {
        setDateTime(dateTime);
        return this;
    }

//    public AnswerComment withUserId(int userId) {
//        setUserId(userId);
//        return this;
//    }

    public AnswerComment build() {
        //action
        return this;
    }

}
