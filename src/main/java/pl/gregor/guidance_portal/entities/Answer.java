package pl.gregor.guidance_portal.entities;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * Question entity
 * @author Grzegorz Bilas
 *
 */

@Getter @Setter @EqualsAndHashCode @NoArgsConstructor
//@Builder( buildMethodName = "build", builderMethodName = "", toBuilder = false, access = AccessLevel.PUBLIC, setterPrefix = "with")
//@Builder(builderClassName = "Question", buildMethodName = "build", builderMethodName = " ", toBuilder = false, access = AccessLevel.PUBLIC, setterPrefix = "with")
@Entity
@Table(name = "answers")
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    private int id;

    @Column(name = "content_answer")
    private String contentAnswer;

    @Column(name = "date_time")
    private Date dateTime;

    @Column(name = "ranking")
    private int ranking;

    @ManyToOne
    private Question question;

    @ManyToOne
    private User user_an;

    @OneToMany(mappedBy="answer", cascade = CascadeType.ALL)
    private List<AnswerComment> answerCommList;

    public Answer withId(int id) {
        setId(id);
        return this;
    }

    public Answer withContentComment(String contentAnswer) {
        setContentAnswer(contentAnswer);
        return this;
    }

    public Answer withDateTime(Date dateTime) {
        setDateTime(dateTime);
        return this;
    }

    public Answer withRanking(int ranking) {
        setRanking(ranking);
        return this;
    }

//    public Answer withUserId(User user) {
//        setUser(user);
//        return this;
//    }

    public Answer build() {
        //action
        return this;
    }

}
