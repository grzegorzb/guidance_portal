package pl.gregor.guidance_portal.entities;

import lombok.*;

import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * Question entity
 * @author Grzegorz Bilas
 *
 */

@Getter @Setter @EqualsAndHashCode @NoArgsConstructor
//@Builder( buildMethodName = "build", builderMethodName = "", toBuilder = false, access = AccessLevel.PUBLIC, setterPrefix = "with")
//@Builder(builderClassName = "Question", buildMethodName = "build", builderMethodName = " ", toBuilder = false, access = AccessLevel.PUBLIC, setterPrefix = "with")
@Entity
@Table(name = "questions")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    private int id;

    @Column(name = "description")
    private String description;

    @Column(name = "date_time")
    private Date dateTime;

    @ManyToOne
    private Category cat_ref;

    @ManyToOne
    private User user_qu;

    @OneToMany(mappedBy="question", cascade = CascadeType.ALL)
    private List<Answer> answerList;

    public Question withId(int id) {
        setId(id);
        return this;
    }

    public Question withDescription(String description) {
        setDescription(description);
        return this;
    }

    public Question withDateTime(Date dateTime) {
        setDateTime(dateTime);
        return this;
    }

    public Question build() {
        //action
        return this;
    }

}
