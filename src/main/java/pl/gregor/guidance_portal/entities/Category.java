package pl.gregor.guidance_portal.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;


/**
 * Category entity
 * @author Grzegorz Bilas
 *
 */

@Getter
@Setter
@Entity
@Table(name = "categories")
public class Category {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE) //(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;


    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy="cat_ref", cascade = CascadeType.ALL)
    private List<Question> questList;

    @OneToMany(mappedBy="primary_category", cascade = CascadeType.ALL)
    private List<Category> catList;

    @ManyToOne
    private Category primary_category;

    public Category() {
    }

    public Category withId(int id) {
        setId(id);
        return this;
    }

    public Category withName(String name) {
        setName(name);
        return this;
    }

    public Category build() {
        //action
        return this;
    }

//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
}
