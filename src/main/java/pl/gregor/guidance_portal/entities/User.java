package pl.gregor.guidance_portal.entities;

import lombok.*;

import javax.persistence.*;
import java.util.List;


/**
 * User entity
 * @author Grzegorz Bilas
 *
 */

@Getter @Setter @EqualsAndHashCode @NoArgsConstructor
@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "password")
    private String pass;

    @Column(name = "email")
    private String email;

    @OneToMany(mappedBy="user_qu", cascade = CascadeType.ALL)
    private List<Question> questList;

    @OneToMany(mappedBy="user_an", cascade = CascadeType.ALL)
    private List<Answer> answerList;

    @OneToMany(mappedBy="user_co", cascade = CascadeType.ALL)
    private List<AnswerComment> answerCommList;

    public User withId(int id) {
        setId(id);
        return this;
    }

    public User withName(String name) {
        setName(name);
        return this;
    }

    public User withEmail(String email) {
        setName(email);
        return this;
    }

    public User withPass(String pass) {
        setName(pass);
        return this;
    }

    public User build() {
        //action
        return this;
    }

}
