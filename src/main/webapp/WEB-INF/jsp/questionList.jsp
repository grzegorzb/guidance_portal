<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<h2>Question List:</h2>

<table>
    <tr>
        <th>Treść pytania</th>
        <th></th>
    </tr>
    <c:forEach var="question" items="${questions}">
        <tr>
            <td>${question.description}</td>
            <td><a href="/question/${question.id}">
                <input type="button" value="Zobacz"/></a></td>
        </tr>
    </c:forEach>
</table>
<input type="hidden" name="${csrf.parameterName}" value="${csrf.token}"/>
<%--<a href="/newQuestion"> <input type="button" value="Zarejestruj NOWA"/></a>--%>