<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:set var = "quest_time" value = "${fn:substring(question.dateTime, 0, 16)}" />
<h3>One Question:</h3>

<table>
    <tr>
        <th> <h3>Kategoria:</h3> </th>  <th> &ensp </th>  <th> <h2>${category_name}</h2> </th>
    </tr>
</table>

${question.description}<br/>
Pytanie zostalo zadane: ${quest_time}<br/>

<table>
    <tr>
        <th> </th> <th> </th>
        <th>Odpowiedzi:</th>
    </tr>
    <c:forEach var="answer" items="${answers}">
        <tr>
            <td>${answer.ranking}</td>
            <td>${answer.date_time}</td>
            <td>${answer.content_answer}</td>
            <td><a href="/answer/${answer.id}"><input type="button" value="Wybierz"/></a></td>
        </tr>
    </c:forEach>
</table>

<p>Dzisiaj jest: <%= java.time.LocalDate.now().toString() %></p>
<input type="hidden" name="${csrf.parameterName}" value="${csrf.token}"/>
<%--<a href="/newQuestion"> <input type="button" value="Zarejestruj NOWA"/></a>--%>