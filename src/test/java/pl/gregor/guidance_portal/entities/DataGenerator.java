package pl.gregor.guidance_portal.entities;

import org.junit.Test;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import pl.gregor.guidance_portal.util.GenericDao;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Test data generator
 * @author Grzegorz Bilas
 *
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class DataGenerator {
    static SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    static Category categoryMainComputers = new Category();
    static Category categoryHardware = new Category();
    static Category categoryFlowers = new Category();
    static Category categorySoftware = new Category();

    static Question quest1 = new Question();
    static Question quest2 = new Question();
    static Question quest3 = new Question();
    static Question quest4 = new Question();
    static Question quest5 = new Question();
    static Question quest6 = new Question();

    static User user1 = new User();
    static User user2 = new User();
    static User user3 = new User();

    static Answer answ_storczyk = new Answer();
    static Answer answ_linux = new Answer();
    static Answer answ3 = new Answer();
    static Answer answ4 = new Answer();
    static Answer answ5 = new Answer();

    @Test
    public void g02_category() {
        GenericDao<Category> dao = new GenericDao();

//        categoryMainComputers = new Category();
        categoryMainComputers.setName("Komputery");
        dao.save(categoryMainComputers);

//        categoryHardware = new Category();
        categoryHardware.setName("Sprzęt");
        categoryHardware.setPrimary_category(categoryMainComputers);
        dao.save(categoryHardware);

//        categoryFlowers = new Category();
        categoryFlowers.setName("Kwiaty");
        dao.save(categoryFlowers);

//        categorySoftware = new Category();
        categorySoftware.setName("Oprogramowanie");
        categorySoftware.setPrimary_category(categoryMainComputers);
        dao.save(categorySoftware);

    }

    @Test
    public void g03_user() {
        GenericDao<User> dao = new GenericDao();

//        user1 = new User();
        user1.setName("Roman");
        user1.setEmail("romek@atomek.pl");
        user1.setPass("atomek");
        dao.save(user1);

//        user2 = new User();
        user2.setName("Krystek");
        user2.setEmail("krystek@gdynia.pl");
        user2.setPass("gdynia");
        dao.save(user2);

//        user3 = new User();
        user3.setName("Sandra");
        user3.setEmail("sandra@hotel.pl");
        user3.setPass("hotel");
        dao.save(user3);
    }

    @Test
    public void g04_question() throws ParseException {
        GenericDao<Question> dao_q = new GenericDao();

//        quest1 = new Question();
        quest1.setDescription("Jaki Windows wybrać dla gracza?");
        quest1.setCat_ref(categorySoftware);
        quest1.setDateTime(new Date());
        quest1.setUser_qu(user2);
        dao_q.save(quest1);

//        quest2 = new Question();
        quest2.setDescription("Gdzie posadzić storczyk, żeby zakwitł wiosną?");
        quest2.setCat_ref(categoryFlowers);
        quest2.setDateTime(ft.parse("2020-11-16 16:34:56"));
        quest2.setUser_qu(user3);
        dao_q.save(quest2);

//        quest3 = new Question();
        quest3.setDescription("Czy można ogórki wysadzać do doniczki?");
        quest3.setCat_ref(categoryFlowers);
        quest3.setDateTime(ft.parse("2020-10-17 07:34:56"));
        quest3.setUser_qu(user2);
        dao_q.save(quest3);

//        quest4 = new Question();
        quest4.setDescription("Która dystrybucja Linuxa korzysta z apt-get?");
        quest4.setCat_ref(categorySoftware);
        quest4.setDateTime(ft.parse("2020-07-17 02:34:56"));
        quest4.setUser_qu(user1);
        dao_q.save(quest4);

//        quest5 = new Question();
        quest5.setDescription("Czy MS teams działają z Wndows Vista?");
        quest5.setCat_ref(categorySoftware);
        quest5.setDateTime(ft.parse("2020-11-17 20:34:56"));
        quest5.setUser_qu(user1);
        dao_q.save(quest5);

//        quest6 = new Question();
        quest6.setDescription("Jaki laptop polecacie dla gracza?");
        quest6.setCat_ref(categoryHardware);
        quest6.setDateTime(ft.parse("2020-11-17 20:34:56"));
        quest6.setUser_qu(user3);
        dao_q.save(quest6);
    }

    @Test
    public void g05_Answer() throws ParseException {
        GenericDao<Answer> dao = new GenericDao();

        answ_storczyk.setContentAnswer("Kwiatek posadź na balkonie w słonecznym miejscu");
        answ_storczyk.setRanking(4);
        answ_storczyk.setQuestion(quest2);
        answ_storczyk.setUser_an(user2);
        answ_storczyk.setDateTime(ft.parse("2020-11-18 22:24:46"));
        dao.save(answ_storczyk);

        answ_storczyk.setContentAnswer("Ważna jest żyzna gleba i wysokim PH i oczywiście słoneczko.");
        answ_storczyk.setRanking(3);
        answ_storczyk.setQuestion(quest2);
        answ_storczyk.setUser_an(user3);
        answ_storczyk.setDateTime(ft.parse("2020-12-04 22:24:46"));
        dao.save(answ_storczyk);

        answ_linux.setContentAnswer("W Red Hat tego nie mam.");
        answ_linux.setRanking(4);
        answ_linux.setQuestion(quest4);
        answ_linux.setUser_an(user3);
        answ_linux.setDateTime(ft.parse("2020-11-18 22:24:46"));
        dao.save(answ_linux);
    }

    @Test
    public void g06_AnswerComment() throws ParseException {
        GenericDao<AnswerComment> dao = new GenericDao();

        AnswerComment answComm = new AnswerComment();
        answComm.setContentComment("Można też wysadzić do gruntu ale w suchym nasłonecznionym miejscu");
        answComm.setAnswer(answ_storczyk);
        answComm.setDateTime(ft.parse("2020-11-19 20:34:56"));
        answComm.setUser_co(user1);
        dao.save(answComm);

        answComm.setContentComment("U mnie te kwiatki w gruncie nie zakwitły wiosną.");
        answComm.setDateTime(new Date());
        answComm.setUser_co(user3);
        dao.save(answComm);
    }
}